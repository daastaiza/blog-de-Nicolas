@extends('layouts.app')

@section('header')

@endsection

@section('content')

<section class="main-section" id="service"><!--main-section-start-->
    <div class="container">
        <div class="row">
            @include('flash::message')
                @foreach ($information as $information)

            <h2>
                <p>Informacion Personal
                    @if (Auth::guest())
                        @else
                            <a data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil " aria-hidden="true"></i></a>
                        @endif
                </p>
            </h2>
            <h3><p>{{$information->personal_information}}</p></h3>

            <h2><p>Informacion academica</p></h2>

            <h3><p>{{$information->acedemic_information }}</p></h3>

            <h2><p>Mas obre mi</p></h2>

            {{$information->more_informacion }}
            @endforeach
        </div>
    </div>
</section><!--main-section-end-->


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar mi Informacion</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => ['Information.update', $information->id], 'method' => 'PUT']) !!}
                

                <div class="form-group">
                    {!! Form::label('personal_information', 'Informacion Personal') !!}
                    {!! Form::textarea('personal_information', $information->personal_information, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                </div>
                                        
                <div class="form-group">
                    {!! Form::label('acedemic_information', 'Informacion Academica') !!}
                    {!! Form::textarea('acedemic_information', $information->acedemic_information, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('more_informacion', 'Mas informacion') !!}
                    {!! Form::textarea('more_informacion', $information->more_informacion, ['class' =>'form-control textarea-more', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                </div>

                <div class="form-group">
                    {!! Form::hidden('blog_id', $information->blog_id) !!}
                </div>

                <div class="modal-footer">
                    {!! Form::submit('Guardar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                </div>

            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

    
@endsection

@section('js')
<script>
    $('.textarea-academic').trumbowyg();
    $('.textarea-personal').trumbowyg();
    $('.textarea-more').trumbowyg();
</script>
@endsection