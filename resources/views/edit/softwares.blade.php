@extends('layouts.app')

@section('header')

@endsection

@section('content')
<section class="main-section" id="service">

                    <div class="container">
                        @include('flash::message')
                            {!! Form::open(['route' => ['software.update', $software->id], 'method' => 'PUT', 'files' => true]) !!}
                               
                               <div class="form-group">
                                    {!! Form::label('name_project', 'Nombre') !!}
                                    {!! Form::text('name', $software->name, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del portafolio']) !!}
                                </div>
                                                        
                                <div class="form-group">
                                    {!! Form::label('description_project', 'Descripcion del portafolio') !!}
                                    {!! Form::textarea('description', $software->description, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del portafolio']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('url', 'URL') !!}
                                    {!! Form::text('url', $software->url, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del portafolio']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::hidden('blog_id', 1) !!}
                                </div>

                                <center>
                                    {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
                                </center>
                                    
                                
                            {!! Form::close() !!}
                    </div>
                        
                

                
    </section>
@endsection

@section('js')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.textarea-academic').trumbowyg();
</script>
@endsection