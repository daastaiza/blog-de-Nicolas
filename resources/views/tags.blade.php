@extends('layouts.app')

@section('header')

@endsection

@section('content')
@if (Auth::guest())
    @else
        <div class="pull-right">
             <a data-toggle="modal" data-target="#New" data-placement="bottom" title="Nuevo"><i class="btn fa fa-plus fa-4x" aria-hidden="true"></i></a>
        </div>
@endif
    <section class="main-section" id="service">
        <center>
            <h1>Mis Tags</h1>
        </center>
        <div class="container">
            <div class="row">
                @include('flash::message')
                @foreach ($tag as $tag)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center>
                                    <h3><a href="#">{{$tag->name}}</a></h3>
                                </center>
                            </div>
                            
                                <div class="panel-footer"> 
                                    <center>
                                        @if (Auth::guest())
                                            @else
                                                <a href="{{route('Tag.edit', $tag->id)}}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil fa-2x" aria-hidden="true"></i></a>
                                                <a href="{{route('Tag.destroy', $tag->id)}}" data-toggle="tooltip" data-placement="bottom" title="Eliminar"><i class="btn fa fa-trash fa-2x" aria-hidden="true"></i></a> 
                                            @endif
                                            
                                            
                                    </center>
                                    </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


<!-- Agregar al portafolio -->
  <div class="modal fade" id="New" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo tag</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => 'Tag.store', 'method' => 'POST', 'files' => true]) !!}
                

                <div class="form-group">
                    {!! Form::label('name_project', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del tag']) !!}
                </div>
        </div>
        <div class="modal-footer">
            <center>
                {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
            </center>
          {!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>

@endsection

@section('js')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.textarea-academic').trumbowyg();
</script>
@endsection