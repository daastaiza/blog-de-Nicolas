@extends('layouts.app')

@section('header')

@endsection

@section('content')
@if (Auth::guest())
@else
    <div class="pull-right">
         <a data-toggle="modal" data-target="#New"><i class="btn fa fa-plus fa-4x" aria-hidden="true"></i></a>
    </div>
@endif
    <section class="main-section" id="service">
         
        <div class="container">
            <div class="row">
                @include('flash::message')
                @foreach ($image as $image)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @if (Auth::guest())
                                    <h3><a href="{{route('Portafolio.edit', $image->id)}}">{{$image->portafolio->name_project}}</a></h3>
                                @else
                                    <h3><a href="#">{{$image->portafolio->name_project}}</a></h3>
                                @endif
                                
                            </div>
                            <div class="panel-body">
                                <img src="/images/portafolio/{{$image->name}}" >
                                <div>
                                    <p>{{$image->portafolio->description_project}}</p>
                                </div>
                            </div>
                            @if (Auth::guest())
                                @else
                                    <div class="panel-footer">
                                        <center>
                                            <a href="{{route('Portafolio.edit', $image->id)}}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil fa-2x" aria-hidden="true"></i></a>
                                            <a href="{{route('Portafolio.destroy', $image->id)}}" data-toggle="tooltip" data-placement="bottom" title="Eliminar"><i class="btn fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                        </center>
                                    </div>
                                @endif
                            
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

<!--
    Mis Modelas
-->
<!-- Agregar al portafolio -->
  <div class="modal fade" id="New" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo folder</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => 'Portafolio.store', 'method' => 'POST', 'files' => true]) !!}
                

                <div class="form-group">
                    {!! Form::label('name_project', 'Nombre') !!}
                    {!! Form::text('name_project', null, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del portafolio']) !!}
                </div>
                                        
                <div class="form-group">
                    {!! Form::label('description_project', 'Descripcion del portafolio') !!}
                    {!! Form::textarea('description_project', null, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del portafolio']) !!}
                </div>

                <div class="form-group">
                    {!! Form::hidden('blog_id', 1) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image','Imagen') !!}
                    {!! Form::file('image', ['requerid']) !!}
                </div>
                
                
            
        </div>
        <div class="modal-footer">
            <center>
                {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
            </center>
          {!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>

@endsection

@section('js')
<script>
    $('.textarea-academic').trumbowyg();
</script>
@endsection