<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class perfil extends Model
{
     protected $table =  'perfil';
   protected $fillable = ['general_information','portfolio_information','images','fk_page_id'];
   protected  $guarded = ['id'];
}
