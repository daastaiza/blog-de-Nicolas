<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class portfolio extends Model
{
	 protected $table =  'portfolio';
   protected $fillable = ['portfolio_name','description','images_portfolio'];
   protected  $guarded = ['id'];
    }
