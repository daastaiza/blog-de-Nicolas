<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{
     protected $table =  'new';
   protected $fillable = ['name','information_article','fk_type_id'];
   protected  $guarded = ['id'];
}
