<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\news;
use App\Models\perfil;
use App\Models\portfolio;
use App\Models\type;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticia = news::all();
        $perfil = perfil::all();
        $portafolio = portfolio::all();
        $type = type::all();
        
        return view('home')
        ->with('noticia',$noticia)
        ->with('perfil', $perfil)
        ->with('portafolio',$portafolio)
        ->with('type',$type);


    }
    public function show($id)
    {
        $perfil = perfil::find($id);

        return view('perfiledit')
        ->with('perfil', $perfil);
    }
}
